﻿1
00:00:05,400 --> 00:00:09,280
Vi har et matsystem som er lite bærekraft i dag. 

2
00:00:09,520 --> 00:00:14,040
Vi kaster mat som kunnet mettet
800.000 mennesker

3
00:00:14,080 --> 00:00:15,680
hvert eneste år i Norge. 

4
00:00:16,080 --> 00:00:20,440
Jeg heter Anja Bakken Riise,
er 31 år gammel, fra Tromsø

5
00:00:20,520 --> 00:00:22,840
og leder Framtiden i våre hender. 

6
00:00:23,880 --> 00:00:29,680
En kilo kjøtt kan kreve flere tusen liter vann. 

7
00:00:30,240 --> 00:00:32,920
Samtidig vet vi at veldig mange mennesker i verden

8
00:00:33,000 --> 00:00:35,120
mangler tilgang på rent vann. 

9
00:00:35,120 --> 00:00:39,840
Dyrene har gjerne spist kraftfòr som vi har
importert fra Brasil

10
00:00:39,840 --> 00:00:43,880
som ofte fører til ødeleggelse 
av regnskogområder

11
00:00:43,880 --> 00:00:48,720
og som ofte driver fattige bønder 
bort fra sine områder. 

12
00:00:51,360 --> 00:00:58,480
I fjor, på strandryddedagen, så man at en fjerdedel
av plasten man fant var matemballasje.

13
00:00:58,480 --> 00:01:02,800
Noe av den plasten har vært bra.
Den har kuttet mye matsvinn.

14
00:01:02,800 --> 00:01:05,240
Men jeg tror det er veldig mye unødvendig plast

15
00:01:05,400 --> 00:01:07,720
Sam matvarebransjen bare må slutte å bruke.

16
00:01:11,040 --> 00:01:13,680
Besteforeldrene våre var mye flinkere til å 

17
00:01:13,680 --> 00:01:15,520
konservere og å ta vare på maten.

18
00:01:15,600 --> 00:01:18,160
Vi har begynt å stole mer på datomerkingen

19
00:01:18,400 --> 00:01:19,800
enn på våre egne sanser.

20
00:01:22,240 --> 00:01:23,880
Flere blir med i Too good to go.

21
00:01:23,880 --> 00:01:26,400
Vi prøver å redusere vår egen matkasting.

22
00:01:26,680 --> 00:01:30,720
Vi kutter ut kjøtt en eller to eller tre
dager i uka.

23
00:01:30,920 --> 00:01:32,960
Eller vi begynner å dyrke vår egen mat.

24
00:01:33,080 --> 00:01:34,440
Så det er mye bra som skjer

25
00:01:34,560 --> 00:01:38,760
og det er først og fremst fordi så utrolig mange 
vanlige folk har engasjert seg.

26
00:01:38,880 --> 00:01:40,680
Hvis mange nok engasjerer seg

27
00:01:40,840 --> 00:01:42,920
så kan vi få til utrolig store endringer.
