require('dotenv').config();

module.exports = {
  modules: [
    '@nuxtjs/axios',
  ],
  /*
  ** Headers of the page
  */
  head: {
    title: 'Automatiseringsteknikk',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Automatiseringsteknikk' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://unpkg.com/cloudinary-video-player/dist/cld-video-player.min.css' }
    ]
  },
  plugins: [
    '~/plugins/axios',
    { src: '~/plugins/vue-scrollto', ssr: false },
    { src: '~/plugins/vue-js-modal', ssr: false },
    { src: '~/plugins/accordion', ssr: false },
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#2E2D84' },
  /*
  ** Build configuration
  */
  env: {
    baseUrl: process.env.BASE_URL,
    baseNode: process.env.BASE_NODE,
    baseTree: process.env.BASE_TREE
  },
  generate: {
    routes: [
      '',
      '/',
      '/bm/',
      '/bm/tidslinje/1814/',
      '/bm/tidslinje/1814-1905/',
      '/bm/filmer/',
      '/bm/oppgaver/',
      '/bm/A1/',
      

      '/bm/oppgaver/1/1',
      '/bm/oppgaver/1/2',
      '/bm/oppgaver/1/3',
      '/bm/oppgaver/1/4',
      '/bm/oppgaver/1/5',
      '/bm/oppgaver/1/6',
      '/bm/oppgaver/1/7',

      '/bm/oppgaver/2/1',
      '/bm/oppgaver/2/2',
      '/bm/oppgaver/2/3',
      '/bm/oppgaver/2/4',

      '/bm/oppgaver/3/1',
      '/bm/oppgaver/3/2',
      '/bm/oppgaver/3/3',

      '/bm/oppgaver/4/1',
      '/bm/oppgaver/4/2',

      '/bm/oppgaver/5/1',
      '/bm/oppgaver/5/2',
      '/bm/oppgaver/5/3',
      '/bm/oppgaver/5/4',

      '/bm/oppgaver/6/1',
      '/bm/oppgaver/6/2',
    ]
  },
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
